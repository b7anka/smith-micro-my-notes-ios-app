//
//  My_Notes_TestUITests.swift
//  My Notes TestUITests
//
//  Created by João Moreira on 29/03/2021.
//

import XCTest

class My_Notes_TestUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }
    
    func test0RegisterView() throws {

        let app = XCUIApplication()
        app.launch()
        
        let loginRegisterButton = app.buttons[AccessibilityIdentifiers.loginRegisterButton]
        XCTAssert(loginRegisterButton.exists)
        loginRegisterButton.tap()
        
        app.navigationBars["Register"].buttons["Back"].tap()
        
        loginRegisterButton.tap()

        let avatarButton = app.buttons[AccessibilityIdentifiers.registerImageSelectionButton]
        XCTAssert(avatarButton.exists)
        avatarButton.tap()
        
        let emailLabel = app.staticTexts[AccessibilityIdentifiers.registerEmailLabel]
        XCTAssert(emailLabel.exists)
        
        let passwordLabel = app.staticTexts[AccessibilityIdentifiers.registerPasswordLabel]
        XCTAssert(passwordLabel.exists)
        
        let confirmPasswordLabel = app.staticTexts[AccessibilityIdentifiers.registerConfirmPasswordLabel]
        XCTAssert(confirmPasswordLabel.exists)

        let emailTextField = app.textFields[AccessibilityIdentifiers.registerEmailTextField]
        XCTAssert(emailTextField.exists)
        emailTextField.tap()
        emailTextField.typeText("email@example.com")
        XCTAssertEqual(emailTextField.value as! String, "email@example.com")
        
        let passwordTextField = app.secureTextFields[AccessibilityIdentifiers.registerPasswordTextField]
        XCTAssert(passwordTextField.exists)
        passwordTextField.tap()
        passwordTextField.typeText("email@example.com")
        
        let confirmPasswordTextField = app.secureTextFields[AccessibilityIdentifiers.registerConfirmPasswordTextField]
        XCTAssert(confirmPasswordTextField.exists)
        confirmPasswordTextField.tap()
        confirmPasswordTextField.typeText("email@example.com")
        
        if app.keyboards.element(boundBy: 0).exists {
            app.keyboards.buttons["Return"].tap()
        }
        
        let chooseCameraButton = app.buttons[AccessibilityIdentifiers.chooseCameraRegisterButton]
        let chooseLibrary = app.buttons[AccessibilityIdentifiers.chooseFromLibraryRegisterButton]
        XCTAssert(chooseCameraButton.exists)
        XCTAssert(chooseLibrary.exists)
        
        let registerButton = app.buttons[AccessibilityIdentifiers.registerButton]
        XCTAssert(registerButton.exists)
        registerButton.tap()

    }

    func test1LoginView() throws {

        let app = XCUIApplication()
        app.launch()

        let topImage = app.images[AccessibilityIdentifiers.loginImage]
        XCTAssert(topImage.exists)
        
        let emailLabel = app.staticTexts[AccessibilityIdentifiers.loginEmailLabel]
        XCTAssert(emailLabel.exists)
        
        let passwordLabel = app.staticTexts[AccessibilityIdentifiers.loginPasswordLabel]
        XCTAssert(passwordLabel.exists)
        
        let emailTextField = app.textFields[AccessibilityIdentifiers.loginEmailTextField]
        XCTAssert(emailTextField.exists)
        emailTextField.tap()
        emailTextField.typeText("email@example.com")
        XCTAssertEqual(emailTextField.value as! String, "email@example.com")
    
        let passwordTextField = app.secureTextFields[AccessibilityIdentifiers.loginPasswordTextField]
        XCTAssert(passwordTextField.exists)
        passwordTextField.tap()
        passwordTextField.typeText("email@example.com")

        let loginButton = app.buttons[AccessibilityIdentifiers.loginButton]
        XCTAssert(loginButton.exists)
        loginButton.tap()

    }

    func test2LaunchPerformance() throws {
        measure(metrics: [XCTApplicationLaunchMetric()]) {
            XCUIApplication().launch()
        }
    }
    
}
