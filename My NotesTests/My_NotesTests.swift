//
//  My_NotesTests.swift
//  My NotesTests
//
//  Created by João Moreira on 29/03/2021.
//

import XCTest
import SwiftUI
@testable import My_Notes

class My_NotesTests: XCTestCase {

    func testGetBundleInfoPlistValuesFunction() throws {
        let identifier: String? = Utils.shared.getBundleInfoPlistValues(key: AppConstants.bundleIdentifierKey)
        XCTAssert(identifier == "com.tiago-moreira.My-Notes")
    }
    
    func testgenerateQRCodeFunction() throws {
        let image: UIImage? = Utils.shared.generateQRCode(from: "my notes are awesome")
        XCTAssert(image != nil)
    }

    func testGetColorForAlertFuntion() throws {
        let color: Color = Utils.shared.getColorForAlert(type: .error)
        XCTAssert(color == .red)
    }
    
    func testDeviceLanguageFuntion() throws {
        let locale = Utils.shared.getDeviceLanguage()
        let systemLocale = Locale.preferredLanguages.first?.components(separatedBy: "-").first
        XCTAssert(systemLocale != nil && systemLocale == locale)
    }

}
