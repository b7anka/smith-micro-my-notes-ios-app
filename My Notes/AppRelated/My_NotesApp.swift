//
//  My_NotesApp.swift
//  My Notes
//
//  Created by João Moreira on 24/03/2021.
//

import SwiftUI

@main
struct My_NotesApp: App {
    
    @StateObject var screenCoordinator = MNEnviromentCoordinator()

    var body: some Scene {
        WindowGroup {
            NavigationView {
                InitalView()
            }.environmentObject(self.screenCoordinator)
        }
    }
}
