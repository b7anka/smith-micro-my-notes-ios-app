//
//  Persistence.swift
//  sdasd
//
//  Created by João Moreira on 27/03/2021.
//

import CoreData

struct PersistenceController {
    static let shared = PersistenceController()

    let container: NSPersistentContainer

    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "My_Notes")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    func saveNotes(notes: [MNNoteViewModel]) {
        DispatchQueue.main.async {
            for mnNote in notes {
                self.chekIfNoteExists(mnNote: mnNote) { (result) in
                    if result {
                        self.updateNote(mnNote: mnNote)
                    } else {
                        self.saveNote(mnNote: mnNote)
                    }
                }
            }
        }
    }
    
    func saveNote(mnNote: MNNoteViewModel) {
        DispatchQueue.main.async {
            do {
                let entity = NSEntityDescription.entity(forEntityName: "Note", in: self.container.viewContext)
                let note: Note = Note(entity: entity!, insertInto: self.container.viewContext)
                note.id = Int32(mnNote.id)
                note.content = mnNote.content
                note.date = mnNote.note.date
                note.imageBase64 = mnNote.imageBase64
                note.name = mnNote.name
                note.ownerId = mnNote.ownerId
                try self.container.viewContext.save()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func chekIfNoteExists(mnNote: MNNoteViewModel, completion: @escaping (Bool) -> Void) {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Note")
                let predicate = NSPredicate(format: "id = %d", Int32(mnNote.id))
                fetchRequest.predicate = predicate
                let notes = try self.container.viewContext.fetch(fetchRequest)
                if let note = notes.first as? Note {
                    if note.id == mnNote.id {
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                }
            } catch {
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func updateNote(mnNote: MNNoteViewModel) {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Note")
                let predicate = NSPredicate(format: "id = %d", Int32(mnNote.id))
                fetchRequest.predicate = predicate
                let notes = try self.container.viewContext.fetch(fetchRequest)
                if let note = notes.first as? Note {
                    if note.id == mnNote.id {
                        note.content = mnNote.content
                        note.date = mnNote.note.date
                        note.imageBase64 = mnNote.imageBase64
                        note.name = mnNote.name
                        note.ownerId = mnNote.ownerId
                        try self.container.viewContext.save()
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func deleteNote(mnnNote: MNNoteViewModel) {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Note")
                let predicate = NSPredicate(format: "id = %d", Int32(mnnNote.id))
                fetchRequest.predicate = predicate
                if let items = try self.container.viewContext.fetch(fetchRequest) as? [Note] {
                    items.forEach({self.container.viewContext.delete($0)})
                }
                try self.container.viewContext.save()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func deleteNotes() {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Note")
                if let items = try self.container.viewContext.fetch(fetchRequest) as? [Note] {
                    items.forEach({self.container.viewContext.delete($0)})
                    try self.container.viewContext.save()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func getAllNotes(completion: @escaping ([MNNote]?) -> Void) {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Note")
                if let items = try self.container.viewContext.fetch(fetchRequest) as? [Note] {
                    let mnotes: [MNNote] = items.map{($0.mnNote)}
                    completion(mnotes)
                } else {
                    completion(nil)
                }
            } catch {
                completion(nil)
            }
        }
    }
    
    func saveUser(mnUser: MNUserViewModel) {
        DispatchQueue.main.async {
            do {
                let entity = NSEntityDescription.entity(forEntityName: "User", in: self.container.viewContext)
                let user: User = User(entity: entity!, insertInto: self.container.viewContext)
                user.id = Int32(mnUser.id)
                user.email = mnUser.email
                user.imageBase64 = mnUser.imageBase64
                try self.container.viewContext.save()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func deleteUsers() {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
                if let items = try self.container.viewContext.fetch(fetchRequest) as? [User] {
                    items.forEach({self.container.viewContext.delete($0)})
                    try self.container.viewContext.save()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func getUser(completion: @escaping (MNUser?) -> Void) {
        DispatchQueue.main.async {
            do {
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
                if let items = try self.container.viewContext.fetch(fetchRequest) as? [User] {
                    if let user: User = items.first {
                        completion(user.mnUser)
                    } else {
                        completion(nil)
                    }
                } else {
                    completion(nil)
                }
            } catch {
                completion(nil)
            }
        }
    }
    
    func deleteAll() {
        self.deleteNotes()
        self.deleteUsers()
    }
    
}
