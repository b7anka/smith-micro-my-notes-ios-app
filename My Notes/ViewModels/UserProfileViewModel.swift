//
//  UserProfileViewModel.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI

class UserProfileViewModel: ObservableObject {
    
    @Published var userImage: UIImage?
    @Published var userEmail: String = ""
    @Published var logoutButtonEnabled: Bool = true
    var coordinator: MNEnviromentCoordinator?

    func getUserFromRealm() {
        self.coordinator?.persistenceController.getUser { user in
            DispatchQueue.main.async { [weak self] in
                if let user = user {
                    let userVM: MNUserViewModel = MNUserViewModel(user: user)
                    if let image = MNImageManager.shared.decodeBase64Image(userVM.imageBase64) {
                        self?.userImage = image
                    }
                    self?.userEmail = userVM.email
                }
            }
        }
    }
    
    func logout() {
        withAnimation {
            self.coordinator?.logout()
        }
    }
    
}
