//
//  MNAlertViewModel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

class MNAlertViewModel: ObservableObject {
    
    @Published var isClosing: Bool = false
    @Published var isAnimating: Bool = false
    
    private func hideAlert() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.isClosing = true
            self.isAnimating = true
        }
    }
    
    func animate() {
        self.isAnimating = true
        self.hideAlert()
    }
    
    func getOffset() -> CGFloat {
        if self.isAnimating {
            if self.isClosing {
                return -100
            }
            return 0
        } else {
            if self.isClosing {
                return 0
            }
            return -100
        }
    }
    
}
