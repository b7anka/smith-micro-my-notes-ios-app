//
//  MNNoInternetViewModel.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

class MNNoInternetViewModel: ObservableObject {
    
    @Published var text: String = MNLanguageManager.shared.noInternet
    @Published var imageAssetName: String = ImageAssets.noInternetImage
    @Published var backgroundColor: Color = .red
    private let networkManager: MNNetworkManager = MNNetworkManager()
    
    init() {
        self.networkManager.callback = { result in
            self.begin(result: result)
        }
    }
    
    func begin(result: Bool) {
        if result {
            self.text = MNLanguageManager.shared.internetRestored
            self.imageAssetName = ImageAssets.internetRestoredImage
            self.backgroundColor = .green
        }
    }
    
}

