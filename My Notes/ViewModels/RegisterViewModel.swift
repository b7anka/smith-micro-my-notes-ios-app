//
//  RegisterViewModel.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI
import AVFoundation
import Photos

class RegisterViewModel: ObservableObject {
    
    @Published var emailText: String = "" {
        didSet {
            self.checkIfRegisterButtonShouldBeEnabled()
        }
    }
    @Published var passwordText: String = "" {
        didSet {
            self.checkIfRegisterButtonShouldBeEnabled()
        }
    }
    @Published var confirmPasswordText: String = "" {
        didSet {
            self.checkIfRegisterButtonShouldBeEnabled()
        }
    }
    @Published var shouldEnableRegisterButton: Bool = false
    @Published var showImagePickerView: Bool = false
    @Published var image: UIImage?
    @Published var showButtonToPickImage: Bool = false
    var sourceType: UIImagePickerController.SourceType = .camera
    var presentationMode: Binding<PresentationMode>?
    private let networkManager: MNNetworkManager = MNNetworkManager()
    var coordinator: MNEnviromentCoordinator?
    
    init() {
        self.addObservers()
    }
    
    deinit {
        self.removeObservers()
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkIfRegisterButtonShouldBeEnabled), name: .internetConnectivityChanged, object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: .internetConnectivityChanged, object: nil)
    }
    
    func showCamera() {
        withAnimation {
            self.showButtonToPickImage.toggle()
        }
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case.authorized:
            self.setupCamera()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] (resul) in
                if resul {
                    self?.setupCamera()
                }
            }
        case .denied:
            self.coordinator?.showAlert(message: MNLanguageManager.shared.cameraPermissionNotGiven, type: .error)
        default:
            return
        }
    }
    
    private func setupCamera() {
        DispatchQueue.main.async { [weak self] in
            self?.sourceType = .camera
            self?.showImagePickerView.toggle()
        }
    }
    
    func showLibrary() {
        withAnimation {
            self.showButtonToPickImage.toggle()
        }
        switch PHPhotoLibrary.authorizationStatus(for: PHAccessLevel.readWrite) {
        case.authorized:
            self.setupLibrary()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] (resul) in
                if resul {
                    self?.setupLibrary()
                }
            }
        case .denied:
            self.coordinator?.showAlert(message: MNLanguageManager.shared.libraryPermissionNotGiven, type: .error)
        default:
            return
        }
    }
    
    private func setupLibrary() {
        DispatchQueue.main.async { [weak self] in
            self?.sourceType = .photoLibrary
            self?.showImagePickerView.toggle()
        }
    }
    
    func register() {
        guard MNInputValidatorManager.shared.isEmailValid(email: self.emailText) else {
            self.coordinator?.showAlert(message: MNLanguageManager.shared.emailInvalid, type: .error)
            return
        }
        guard self.validatePasswords() else {
            self.coordinator?.showAlert(message: MNLanguageManager.shared.passwordsDontMatch, type: .error)
            return
        }
        self.coordinator?.isLoading.toggle()
        let basicData: MNLogin = MNLogin(email: self.emailText, password: self.passwordText)
        var base64Image: String?
        if let image = self.image {
            base64Image = MNImageManager.shared.encodeImageToBase64(image)
        }
        let user: MNRegister = MNRegister(basicData: basicData, imageBase64: base64Image)
        APIClient.shared.register(user: user) { (message) in
            DispatchQueue.main.async { [weak self] in
                self?.coordinator?.isLoading.toggle()
                if let message = message {
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                    if message.type == .success {
                        withAnimation {
                            self?.presentationMode?.wrappedValue.dismiss()
                        }
                    }
                }else {
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                }
            }
        }
    }
    
    func didSelectImage(_ image: UIImage?) {
        self.image = image
        self.showImagePickerView.toggle()
    }
    
    @objc private func checkIfRegisterButtonShouldBeEnabled() {
        if !self.emailText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && !self.passwordText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && !self.confirmPasswordText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && (self.coordinator?.hasInternetConnection ?? false) {
            self.shouldEnableRegisterButton = true
        } else {
            self.shouldEnableRegisterButton = false
        }
    }

    private func validatePasswords() -> Bool {
        return self.passwordText == self.confirmPasswordText
    }
    
}
