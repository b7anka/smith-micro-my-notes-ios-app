//
//  MNUserViewModel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNUserViewModel {
    
    let user: MNUser
    
    var id: Int {
        return self.user.id ?? 0
    }
    
    var email: String {
        return self.user.email ?? ""
    }
    
    var imageBase64: String {
        return self.user.imageBase64 ?? ""
    }
    
}

extension MNUserViewModel: Equatable {
    
    static func == (lhs: MNUserViewModel, rhs: MNUserViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}
