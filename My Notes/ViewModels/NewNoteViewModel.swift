//
//  NewNoteViewModel.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI
import Photos

class NewNoteViewModel: ObservableObject {
    
    @Published var noteNameText: String = "" {
        didSet {
            self.checkIfSaveButtonSouldBeEnabled()
        }
    }
    @Published var noteContentText: String = "" {
        didSet {
            self.checkIfSaveButtonSouldBeEnabled()
        }
    }
    @Published var presentImagePickerView: Bool = false
    @Published var showImagePickerButtons: Bool = false
    @Published var enableSaveButton: Bool = false
    var sourceType: UIImagePickerController.SourceType = .camera
    var callback: ((MNNoteViewModel) -> Void) = {_ in}
    @Published var noteImage: UIImage?
    var coordinator: MNEnviromentCoordinator?
    var note: MNNoteViewModel? {
        didSet {
            self.checkIfIsToEdit()
        }
    }
    var presentationMode: Binding<PresentationMode>?
    
    init() {
        self.addObservers()
    }
    
    deinit {
        self.note = nil
        self.removeObservers()
    }
    
    private func checkIfIsToEdit() {
        if let note = self.note {
            self.noteNameText = note.name
            self.noteContentText = note.content
            if let image = MNImageManager.shared.decodeBase64Image(note.imageBase64) {
                self.noteImage = image
            }
        }
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkIfSaveButtonSouldBeEnabled), name: .internetConnectivityChanged, object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: .internetConnectivityChanged, object: nil)
    }
    
    func didSelectImage(_ image: UIImage?) {
        self.noteImage = image
        withAnimation {
            self.presentImagePickerView.toggle()
        }
    }
    
    func showPickerButtons() {
        withAnimation {
            self.showImagePickerButtons.toggle()
        }
    }
    
    func showCamera() {
        withAnimation {
            self.showImagePickerButtons.toggle()
        }
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case.authorized:
            self.setupCamera()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] (resul) in
                if resul {
                    self?.setupCamera()
                }
            }
        case .denied:
            self.coordinator?.showAlert(message: MNLanguageManager.shared.cameraPermissionNotGiven, type: .error)
        default:
            return
        }
    }
    
    private func setupCamera() {
        DispatchQueue.main.async { [weak self] in
            self?.sourceType = .camera
            self?.presentImagePickerView.toggle()
        }
    }
    
    func showLibrary() {
        withAnimation {
            self.showImagePickerButtons.toggle()
        }
        switch PHPhotoLibrary.authorizationStatus(for: PHAccessLevel.readWrite) {
        case.authorized:
            self.setupLibrary()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] (resul) in
                if resul {
                    self?.setupLibrary()
                }
            }
        case .denied:
            self.coordinator?.showAlert(message: MNLanguageManager.shared.libraryPermissionNotGiven, type: .error)
        default:
            return
        }
    }
    
    func saveNote() {
        if let note = self.note {
            self.editNote(note: note)
        } else {
            self.createNewNote()
        }
    }
    
    private func createNewNote() {
        self.coordinator?.isLoading.toggle()
        var note: MNNote = MNNote()
        note.name = self.noteNameText
        note.content = self.noteContentText
        if let image = self.noteImage {
            note.imageBase64 = MNImageManager.shared.encodeImageToBase64(image)
        }
        APIClient.shared.createNote(note: note) { (newNote, message) in
            DispatchQueue.main.async { [weak self] in
                self?.coordinator?.isLoading.toggle()
                if let newNote = newNote {
                    let noteVM: MNNoteViewModel = MNNoteViewModel(note: newNote)
                    self?.coordinator?.persistenceController.saveNote(mnNote: noteVM)
                    self?.callback(noteVM)
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.noteCreatedSuccessfully, type: .success)
                    self?.presentationMode?.wrappedValue.dismiss()
                } else if let message = message {
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                } else {
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                }
            }
        }
    }
    
    private func editNote(note: MNNoteViewModel) {
        print(note.name)
        self.coordinator?.isLoading.toggle()
        var note: MNNote = note.note
        note.name = self.noteNameText
        note.content = self.noteContentText
        if let image = self.noteImage {
            note.imageBase64 = MNImageManager.shared.encodeImageToBase64(image)
        }
        APIClient.shared.editNote(note: note) { (updatedNote, message) in
            DispatchQueue.main.async { [weak self] in
                self?.coordinator?.isLoading.toggle()
                if let newNote = updatedNote {
                    let noteVM: MNNoteViewModel = MNNoteViewModel(note: newNote)
                    self?.coordinator?.persistenceController.updateNote(mnNote: noteVM)
                    print(noteVM.name)
                    self?.callback(noteVM)
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.noteEditSuccessfully, type: .success)
                    self?.presentationMode?.wrappedValue.dismiss()
                } else if let message = message {
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                } else {
                    self?.coordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                }
            }
        }
    }
    
    private func setupLibrary() {
        DispatchQueue.main.async { [weak self] in
            self?.sourceType = .photoLibrary
            self?.presentImagePickerView.toggle()
        }
    }
    
    @objc private func checkIfSaveButtonSouldBeEnabled() {
        if !self.noteNameText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && !self.noteContentText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && (self.coordinator?.hasInternetConnection ?? false) {
            self.enableSaveButton = true
        } else {
            self.enableSaveButton = false
        }
    }
    
}
