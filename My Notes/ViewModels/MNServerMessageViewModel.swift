//
//  MNServerMessageViewModel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNServerMessageViewModel {
    
    let serverMessage: MNServerMessage
    
    var message: String {
        return self.serverMessage.message ?? ""
    }
    
    var type: MNServerMessageType {
        return MNServerMessageType(rawValue: self.serverMessage.type ?? "") ?? .error
    }
    
    var success: Bool {
        return self.serverMessage.success ?? false
    }
    
    var timestamp: Int64 {
        return self.serverMessage.timestamp ?? 0
    }
    
    var status: Int {
        return self.serverMessage.status ?? -1
    }
    
    var error: String {
        return self.serverMessage.error ?? ""
    }
    
    var exception: String {
        return self.serverMessage.exception ?? ""
    }
    
    var path: String {
        return self.serverMessage.path ?? ""
    }
    
}
