//
//  NotesViewModel.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

class NotesViewModel: ObservableObject {
    
    @Published var notesCollection: [MNNoteViewModel] = []
    @Published var isLoading: Bool = false
    @Published var pageToShow: NotesViewNavigation?
    var noteToEdit: MNNoteViewModel?
    private let networkManager: MNNetworkManager = MNNetworkManager()
    var user: MNUserViewModel?
    var screenCoordinator: MNEnviromentCoordinator?
    
    init() {
        self.networkManager.callback = { result in
            self.load()
            self.loadUser()
        }
    }
    
    func load() {
        self.isLoading.toggle()
        if self.networkManager.hasInternetConnection() {
            APIClient.shared.getNotes { (notes, serverMessage) in
                self.handleResultOfFetchingNotes(notes: notes, message: serverMessage, fromCoreData: false)
            }
        } else {
            self.getNotesFromRealm()
        }
    }
    
    private func getNotesFromRealm() {
        self.screenCoordinator?.persistenceController.getAllNotes(completion: { (notes) in
            self.handleResultOfFetchingNotes(notes: notes, message: nil, fromCoreData: true)
        })
    }
    
    func deleteNote(_ note: MNNoteViewModel) {
        self.screenCoordinator?.isLoading.toggle()
        if self.screenCoordinator?.hasInternetConnection ?? false {
            APIClient.shared.deleteNote(note: note.note) { (message) in
                DispatchQueue.main.async { [weak self] in
                    self?.screenCoordinator?.isLoading.toggle()
                    if let message = message {
                        self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                        if message.type == .success {
                            self?.screenCoordinator?.persistenceController.deleteNote(mnnNote: note)
                            if let index = self?.notesCollection.firstIndex(of: note) {
                                self?.notesCollection.remove(at: index)
                            }
                        }
                    } else {
                        self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                    }
                }
            }
        } else {
            self.screenCoordinator?.isLoading.toggle()
            self.screenCoordinator?.showAlert(message: MNLanguageManager.shared.noInternet, type: .error)
        }
    }
    
    func editNote(_ note: MNNoteViewModel) {
        self.noteToEdit = note
        self.pageToShow = .newNote
    }
    
    func shareNote(_ note: MNNoteViewModel) {
        if let id = String(note.id).data(using: .utf8)?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)), let ownerId = note.ownerId.data(using: .utf8)?.base64EncodedString(options:  Data.Base64EncodingOptions(rawValue: 0)), let qrCodeImage: UIImage = Utils.shared.generateQRCode(from: "\(MNConfig.shared.qrCodeKey)\(id)-\(ownerId)") {
            let av = UIActivityViewController(activityItems: ["\(MNLanguageManager.shared.shareNote) \(note.name)", qrCodeImage], applicationActivities: nil)
            UIApplication.shared.windows.first?.rootViewController?.present(av, animated: true, completion: nil)
        }
    }
    
    func didAddNoteFromQRCode(_ value: String) {
        withAnimation {
            self.pageToShow = nil
        }
        self.screenCoordinator?.isLoading.toggle()
        APIClient.shared.addNoteByQrCode(qrCodeData: MNAddByQrCode(data: value)) { (note, message) in
            DispatchQueue.main.async { [weak self] in
                self?.screenCoordinator?.isLoading.toggle()
                if let note = note {
                    let noteVM: MNNoteViewModel = MNNoteViewModel(note: note)
                    self?.screenCoordinator?.persistenceController.saveNote(mnNote: noteVM)
                    self?.didEditNote(noteVM)
                } else if let message = message {
                    self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                } else {
                    self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                }
            }
        }
    }
    
    func showUserProfile() {
        self.pageToShow = .profileView
    }
    
    func createNote() {
        self.noteToEdit = nil
        self.pageToShow = .newNote
    }
    
    func didEditNote(_ note: MNNoteViewModel) {
        print(note.name)
        if !self.notesCollection.contains(note) {
            self.notesCollection.insert(note, at: 0)
        } else {
            if let index = self.notesCollection.firstIndex(of: note) {
                var notes = self.notesCollection
                notes.remove(at: index)
                self.notesCollection = notes
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    notes.insert(note, at: index)
                    self?.notesCollection = notes
                }
            }
        }
    }
    
    func showCameraPreview() {
        withAnimation {
            self.pageToShow = .cameraView
        }
    }
    
    private func handleResultOfFetchingNotes(notes: [MNNote]?, message: MNServerMessageViewModel?, fromCoreData: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.isLoading.toggle()
            if let notes = notes {
                let notesVM: [MNNoteViewModel] = notes.map(MNNoteViewModel.init)
                if !fromCoreData {
                    self?.screenCoordinator?.persistenceController.saveNotes(notes: notesVM)
                }
                self?.notesCollection = notesVM
            } else if let message = message {
                if message.message == AppConstants.userInvalidKey {
                    print(message.message)
                    self?.screenCoordinator?.logout()
                } else {
                    self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                }
            } else {
                self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                self?.getNotesFromRealm()
            }
        }
    }
    
    private func loadUser() {
        self.screenCoordinator?.persistenceController.getUser(completion: { (user) in
            DispatchQueue.main.async { [weak self] in
                if let user = user {
                    let userVM: MNUserViewModel = MNUserViewModel(user: user)
                    self?.user = userVM
                }
            }
        })
    }
    
}
