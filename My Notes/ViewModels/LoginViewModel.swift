//
//  LoginViewModel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

class LoginViewModel: ObservableObject {
    
    @Published var emailText: String = "" {
        didSet {
            self.checkIfLoginButtonShouldBeEnabled()
        }
    }
    @Published var passwordText: String = "" {
        didSet {
            self.checkIfLoginButtonShouldBeEnabled()
        }
    }
    @Published var enabledButton: Bool = false
    @Published var enabledRegisterButton: Bool = true
    @Published var presentNotesPage: Bool = false
    @Published var pageToShow: LoginViewNavigation?
    var screenCoordinator: MNEnviromentCoordinator?
    
    init() {
        self.addObservers()
    }
    
    deinit {
        self.removeObservers()
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkIfLoginButtonShouldBeEnabled), name: .internetConnectivityChanged, object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: .internetConnectivityChanged, object: nil)
    }
    
    @objc private func checkIfLoginButtonShouldBeEnabled() {
        if !self.emailText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && !passwordText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && (self.screenCoordinator?.hasInternetConnection ?? false) {
            self.enabledButton = true
        }else {
            self.enabledButton = false
        }
    }
    
    func login() {
        self.screenCoordinator?.isLoading = true
        APIClient.shared.login(data: MNLogin(email: emailText, password: passwordText)) { (loginResponse, serverMessage) in
            DispatchQueue.main.async { [weak self] in
                self?.screenCoordinator?.isLoading = false
                if let loginResponse = loginResponse  {
                    MNStorageManager.shared.saveUserToken(token: loginResponse.token ?? "")
                    APIClient.shared.headers.token = MNStorageManager.shared.getUserToken()
                    if let user = loginResponse.user {
                        self?.screenCoordinator?.persistenceController.saveUser(mnUser: MNUserViewModel(user: user))
                    }
                    self?.screenCoordinator?.mainView = .notes
                }else if let message = serverMessage {
                    self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.getTranslations(from: message.message), type: message.type)
                }else {
                    self?.screenCoordinator?.showAlert(message: MNLanguageManager.shared.genericError, type: .error)
                }
            }
        }
    }
    
    func register() {
        self.pageToShow = .register
    }
    
    
}
