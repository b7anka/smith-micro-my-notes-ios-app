//
//  MNNoteViewModel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNNoteViewModel {
    
    let note: MNNote
    
    var id: Int {
        return self.note.id ?? -1
    }
    
    var name: String {
        return self.note.name ?? ""
    }
    
    var date: String {
        let timeStamp = Int64(self.note.date ?? "0") ?? 0
        let date = Date(timeIntervalSince1970: TimeInterval(integerLiteral: timeStamp) / 1000.0)
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter.string(from: date)
    }
    
    var ownerId: String {
        return self.note.ownerId ?? ""
    }
    
    var imageBase64: String {
        return self.note.imageBase64 ?? ""
    }
    
    var content: String {
        return self.note.content ?? ""
    }

}

extension MNNoteViewModel: Equatable {
    
    static func == (lhs: MNNoteViewModel, rhs: MNNoteViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}
