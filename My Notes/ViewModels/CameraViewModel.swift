//
//  CameraViewModel.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI
import AVFoundation

class CameraViewModel: NSObject, ObservableObject {
    
    @Published var isTaken: Bool = false
    @Published var session: AVCaptureSession = AVCaptureSession()
    @Published var alertMessage: String?
    @Published var preview: AVCaptureVideoPreviewLayer!
    @Published var isSaved: Bool = false
    var completion: ((String) -> Void) = {_ in}
    var alertType: MNServerMessageType = .error
    
    func checkPermission() {
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case.authorized:
            self.setup()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { (resul) in
                if resul {
                    self.setup()
                }
            }
        case .denied:
            self.showAlert(message: MNLanguageManager.shared.cameraPermissionNotGiven, type: .error)
        default:
            return
        }
        
    }

    
    private func setup() {
        do {
            self.session.beginConfiguration()
            if let device = AVCaptureDevice.default(for: .video) {
                let input = try AVCaptureDeviceInput(device: device)
                if self.session.canAddInput(input) {
                    self.session.addInput(input)
                }
            } else {
                print("not possible to start camera")
            }
            let metadataOutput = AVCaptureMetadataOutput()
            if self.session.canAddOutput(metadataOutput) {
                self.session.addOutput(metadataOutput)
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            }
            self.session.commitConfiguration()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func showAlert(message: String, type: MNServerMessageType) {
        guard self.alertMessage == nil else {return}
        self.alertMessage = message
        self.alertType = type
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { [weak self] in
            self?.alertMessage = nil
        }
    }
    
}

//MARK: - AVCAPTURE METADATA OUTPUT OBJECTS DELEGATE METHODS
extension CameraViewModel: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let object = metadataObjects.first as? AVMetadataMachineReadableCodeObject
        {
            if object.type == AVMetadataObject.ObjectType.qr, let string = object.stringValue {
                self.session.stopRunning()
                if string.contains(MNConfig.shared.qrCodeKey) {
                    self.completion(string)
                } else {
                    // TODO - display error
                    self.session.startRunning()
                }
            }
        }
    }
    
}
