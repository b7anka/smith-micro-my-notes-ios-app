//
//  APIClient.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation
import Alamofire

class APIClient {
    
    static let shared: APIClient = APIClient()
    var headers: MNHeaders = MNHeaders(apiKey: MNConfig.shared.apiKey, token: MNStorageManager.shared.getUserToken())
    
    init() {
        
    }
    
    // MARK: - OPERATIONS REGARDING USER MANAGEMENT
    func login(data: MNLogin, completion: @escaping (MNLoginResponse?, MNServerMessageViewModel?) -> Void) {
        self.postRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.login, parameters: data.toDictionary()) { (response) in
            do {
                if let data = response.data {
                    if response.response?.statusCode == 200 {
                        let loginResponse: MNLoginResponse = try JSONDecoder().decode(MNLoginResponse.self, from: data)
                        completion(loginResponse, nil)
                    }else {
                        let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                        let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                        completion(nil, messageVM)
                    }
                } else {
                    completion(nil, nil)
                }
            } catch {
                completion(nil, nil)
            }
        }
    }
    
    func logout(completion: @escaping (MNServerMessageViewModel?) -> Void) {
        self.postRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.logout, parameters: nil) { (response) in
            do {
                if let data = response.data {
                    let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                    let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                    completion(messageVM)
                } else {
                    completion(nil)
                }
            } catch {
                completion(nil)
            }
        }
    }
    
    func register(user: MNRegister, completion: @escaping (MNServerMessageViewModel?) -> Void) {
        self.postRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.register, parameters: user.toDictionary()) { (response) in
            do {
                if let data = response.data {
                    let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                    let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                    completion(messageVM)
                } else {
                    completion(nil)
                }
            } catch {
                completion(nil)
            }
        }
    }
    
    // MARK: - OPERATIONS REGARDING NOTES MANAGEMENT
    func getNotes(completion: @escaping ([MNNote]?, MNServerMessageViewModel?) -> Void) {
        self.getRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.note, parameters: nil) { (response) in
            do {
                if let data = response.data {
                    if response.response?.statusCode == 200 {
                        let notes: [MNNote] = try JSONDecoder().decode([MNNote].self, from: data)
                        completion(notes, nil)
                    } else {
                        let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                        let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                        completion(nil, messageVM)
                    }
                } else {
                    completion(nil, nil)
                }
            } catch {
                completion(nil, nil)
            }
        }
    }
    
    func createNote(note: MNNote, completion: @escaping (MNNote?, MNServerMessageViewModel?) -> Void) {
        self.postRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.note, parameters: note.toDictionary()) { (response) in
            do {
                if let data = response.data {
                    if response.response?.statusCode == 200 {
                        let newNote: MNNote = try JSONDecoder().decode(MNNote.self, from: data)
                        completion(newNote, nil)
                    } else {
                        let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                        let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                        completion(nil, messageVM)
                    }
                } else {
                    completion(nil, nil)
                }
            } catch {
                completion(nil, nil)
            }
        }
    }
    
    func editNote(note: MNNote, completion: @escaping (MNNote?, MNServerMessageViewModel?) -> Void) {
        self.putRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.note, parameters: note.toDictionary()) { (response) in
            do {
                if let data = response.data {
                    if response.response?.statusCode == 200 {
                        let updatedNote: MNNote = try JSONDecoder().decode(MNNote.self, from: data)
                        completion(updatedNote, nil)
                    } else {
                        let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                        let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                        completion(nil, messageVM)
                    }
                } else {
                    completion(nil, nil)
                }
            } catch {
                completion(nil, nil)
            }
        }
    }
    
    func deleteNote(note: MNNote, completion: @escaping (MNServerMessageViewModel?) -> Void) {
        self.deleteRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.note, parameters: note.toDictionary()) { (response) in
            do {
                if let data = response.data {
                    let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                    let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                    completion(messageVM)
                } else {
                    completion(nil)
                }
            } catch {
                completion(nil)
            }
        }
    }
    
    func addNoteByQrCode(qrCodeData: MNAddByQrCode, completion: @escaping (MNNote?, MNServerMessageViewModel?) -> Void) {
        self.patchRequest(urlString: MNConfig.shared.serverUrl + MNEndpointsConfig.shared.note, parameters: qrCodeData.toDictionary()) { (response) in
            do {
                if let data = response.data {
                    if response.response?.statusCode == 200 {
                        let note: MNNote = try JSONDecoder().decode(MNNote.self, from: data)
                        completion(note, nil)
                    } else {
                        let serverMessage: MNServerMessage = try JSONDecoder().decode(MNServerMessage.self, from: data)
                        let messageVM: MNServerMessageViewModel = MNServerMessageViewModel(serverMessage: serverMessage)
                        completion(nil, messageVM)
                    }
                } else {
                    completion(nil, nil)
                }
            } catch {
                completion(nil, nil)
            }
        }
    }
    
    // MARK: - CRUD OPERATIONS
    private func getRequest(urlString: String, parameters: Parameters?, completion: @escaping (AFDataResponse<Any>) -> Void) {
        _ = AF.request(urlString, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(self.headers.toDictionary())).responseJSON(completionHandler: { (response) in
            completion(response)
        })
    }
    
    private func postRequest(urlString: String, parameters: Parameters?, completion: @escaping (AFDataResponse<Any>) -> Void) {
        _ = AF.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(self.headers.toDictionary())).responseJSON(completionHandler: { (response) in
            completion(response)
        })
    }
    
    private func putRequest(urlString: String, parameters: Parameters?, completion: @escaping (AFDataResponse<Any>) -> Void) {
        _ = AF.request(urlString, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(self.headers.toDictionary())).responseJSON(completionHandler: { (response) in
            completion(response)
        })
    }
    
    private func patchRequest(urlString: String, parameters: Parameters?, completion: @escaping (AFDataResponse<Any>) -> Void) {
        _ = AF.request(urlString, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(self.headers.toDictionary())).responseJSON(completionHandler: { (response) in
            completion(response)
        })
    }
    
    private func deleteRequest(urlString: String, parameters: Parameters?, completion: @escaping (AFDataResponse<Any>) -> Void) {
        _ = AF.request(urlString, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(self.headers.toDictionary())).responseJSON(completionHandler: { (response) in
            completion(response)
        })
    }
    
}
