//
//  MNConfig.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

class MNConfig {
    
    static let shared: MNConfig = MNConfig()
    
    var data: MNAppConfig?
    
    init() {
        self.data = self.parseConfig()
    }
    
    private func parseConfig() -> MNAppConfig? {
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let config: MNAppConfig = try PropertyListDecoder().decode(MNAppConfig.self, from: data)
                return config
            } catch {
                return nil
            }
        }
        return nil
    }
    
    var apiKey: String {
        return self.data?.apiKey ?? ""
    }
    
    var serverUrl: String {
        let bundleIdentifier: String = Utils.shared.getBundleInfoPlistValues(key: AppConstants.bundleIdentifierKey)
        if bundleIdentifier == AppConstants.prodBundleIdentifier {
            return self.data?.prodServer ?? ""
        }
        return self.data?.testServer ?? ""
    }
    
    var qrCodeKey: String {
        return self.data?.qrCodeKey ?? ""
    }
    
}
