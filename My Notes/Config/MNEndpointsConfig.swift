//
//  MNEndpointsConfig.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

class MNEndpointsConfig {
    
    static let shared: MNEndpointsConfig = MNEndpointsConfig()
    
    private var endpoints: MNEndpoints?
    
    var register: String {
        return self.endpoints?.register ?? ""
    }
    
    var login: String {
        return self.endpoints?.login ?? ""
    }
    
    var logout: String {
        return self.endpoints?.logout ?? ""
    }
    
    var users: String {
        return self.endpoints?.users ?? ""
    }
    
    var note: String {
        return self.endpoints?.note ?? ""
    }
    
    init() {
        self.endpoints = self.parseEndpoints()
    }
    
    private func parseEndpoints() -> MNEndpoints? {
        if let path = Bundle.main.path(forResource: "MNEndpoints", ofType: "plist") {
            do {
                let data: Data = try Data(contentsOf: URL(fileURLWithPath: path))
                let endpoints: MNEndpoints = try PropertyListDecoder().decode(MNEndpoints.self, from: data)
                return endpoints
            } catch {
                return nil
            }
        }
        return nil
    }
    
}
