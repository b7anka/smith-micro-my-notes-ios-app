//
//  Enums.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

enum MNServerMessageType: String {
    
    case error
    case success
    
}

enum LoginViewNavigation: String {
    
    case notes
    case register
    
}

enum MainScreenEntryPoint {
    
    case login
    case notes
    
}

enum NotesViewNavigation: Identifiable {
    
    var id: Int {
        get {
            hashValue
        }
    }
    
    case newNote
    case cameraView
    case profileView
    
}
