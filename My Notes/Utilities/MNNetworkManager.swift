//
//  MNNetworkManager.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import Foundation
import Combine
import Hyperconnectivity

class MNNetworkManager {
    
    private let queue: DispatchQueue = DispatchQueue(label: "network")
    private var cancellable: AnyCancellable?
    private var connectivityResult: ConnectivityPublisher.Output?
    var callback: ((Bool) -> Void) = {_ in}
    
    init() {
        self.cancellable = Hyperconnectivity.Publisher()
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { (connectivityResult) in
                print("received completion")
            }, receiveValue: { (result) in
                self.handleResult(result: result)
            })
    }
    
    deinit {
        self.cancellable?.cancel()
    }
    
    
    private func handleResult(result: ConnectivityPublisher.Output) {
        self.connectivityResult = result
        if result.isConnected {
            self.callback(true)
        } else {
            self.callback(false)
        }
    }
    
    func hasInternetConnection() -> Bool {
        return self.connectivityResult?.isConnected ?? false
    }
    
}
