//
//  Utils.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

class Utils {
    
    static let shared: Utils = Utils()
    
    let availableLanguages: [String] = ["en", "pt"]
    
    func getBundleInfoPlistValues(key: String) -> String {
        if let infoPlist = Bundle.main.infoDictionary {
            return infoPlist[key] as? String ?? key
        }
        return key
    }
    
    func getDeviceLanguage() -> String {
        if let locale = Locale.preferredLanguages.first {
            if let lang = locale.components(separatedBy: "-").first {
                return lang
            }
        }
        return AppConstants.defaultLanguage
    }
    
    func getColorForAlert(type: MNServerMessageType) -> Color {
        if type == .error {
            return Color.red
        }else {
            return Color.green
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 15, y: 15)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return self.convert(output)
            }
        }
        return nil
    }
    
    private func convert(_ cmage:CIImage) -> UIImage? {
        let context:CIContext = CIContext(options: nil)
        guard let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent) else { return nil }
        let image:UIImage = UIImage(cgImage: cgImage)
        return image
    }
    
}
