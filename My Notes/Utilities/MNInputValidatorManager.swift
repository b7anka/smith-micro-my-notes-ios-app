//
//  MNInputValidatorManager.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import Foundation

class MNInputValidatorManager {

    static let shared: MNInputValidatorManager = MNInputValidatorManager()
    
    func isEmailValid(email: String) -> Bool {
        let emailRegEx = "(([^<>()\\[\\]\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))"
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
}
