//
//  Notification+Name.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import Foundation

extension Notification.Name {
    
    static let internetConnectivityChanged = Notification.Name(rawValue: "internetConnectivityChanged")
    
}
