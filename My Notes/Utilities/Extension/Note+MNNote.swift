//
//  Note+MNNote.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import Foundation
import CoreData

extension Note {
    
    var mnNote: MNNote {
        var note: MNNote = MNNote()
        note.content = self.content
        note.date = self.date
        note.id = Int(self.id)
        note.imageBase64 = self.imageBase64
        note.name = self.name
        note.ownerId = self.ownerId
        return note
    }
    
}
