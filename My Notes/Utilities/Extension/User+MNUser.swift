//
//  User+MNUser.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import Foundation
import CoreData

extension User {
    
    var mnUser: MNUser {
        var mnUser: MNUser = MNUser()
        mnUser.email = self.email
        mnUser.id = Int(self.id)
        mnUser.imageBase64 = self.imageBase64
        return mnUser
    }
    
}
