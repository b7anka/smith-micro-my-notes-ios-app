//
//  MNServerMessage+BannerStyle.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import Foundation
import NotificationBannerSwift

extension MNServerMessageType {
    
    var banner: BannerStyle {
        return self == MNServerMessageType.error ? BannerStyle.danger : BannerStyle.success
    }
    
}
