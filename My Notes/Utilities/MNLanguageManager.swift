//
//  MNLanguageViewModel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

class MNLanguageManager {
    
    static let shared: MNLanguageManager = MNLanguageManager()
    
    private var data: MNLanguage?
    private var plistDict: [String: String] = [:]
    
    var apiKeyMissing: String {
        return self.data?.apiKeyMissing ?? ""
    }
    
    var emailOrPasswordWrong: String {
        return self.data?.emailOrPasswordWrong ?? ""
    }
    
    var userDoesNotExtist: String {
        return self.data?.userDoesNotExtist ?? ""
    }
    
    var userAlreadyExists: String {
        return self.data?.userAlreadyExists ?? ""
    }
    
    var noPermissionToEditNote: String {
        return self.data?.noPermissionToEditNote ?? ""
    }
    
    var noteDeletedSuccessfully: String {
        return self.data?.noteDeletedSuccessfully ?? ""
    }
    
    var cantDeleteNote: String {
        return self.data?.cantDeleteNote ?? ""
    }
    
    var userInvalid: String {
        return self.data?.userInvalid ?? ""
    }
    
    var deleteButton: String {
        return self.data?.deleteButton ?? ""
    }
    
    var genericError: String {
        return self.data?.genericError ?? ""
    }
    
    var loginButton: String {
        return self.data?.loginButton ?? ""
    }
    
    var enterYourEmail: String {
        return self.data?.enterYourEmail ?? ""
    }
    
    var enterYourPassword: String {
        return self.data?.enterYourPassword ?? ""
    }
    
    var emailPlaceholder: String {
        return self.data?.emailPlaceholder ?? ""
    }
    
    var passwordPlaceholder: String {
        return self.data?.passwordPlaceholder ?? ""
    }
    
    var registerButton: String {
        return self.data?.registerButton ?? ""
    }
    
    var noInternet: String {
        return self.data?.noInternet ?? ""
    }
    
    var internetRestored: String {
        return self.data?.internetRestored ?? ""
    }
    
    var confirmPassword: String {
        return self.data?.confirmPassword ?? ""
    }
    
    var emailInvalid: String {
        return self.data?.emailInvalid ?? ""
    }
    
    var passwordsDontMatch: String {
        return self.data?.passwordsDontMatch ?? ""
    }
    
    var libraryPermissionNotGiven: String {
        return self.data?.libraryPermissionNotGiven ?? ""
    }
    
    var cameraPermissionNotGiven: String {
        return self.data?.cameraPermissionNotGiven ?? ""
    }
    
    var tapToChooseImage: String {
        return self.data?.tapToChooseImage ?? ""
    }
    
    var editButton: String {
        return self.data?.editButton ?? ""
    }
    
    var shareButton: String {
        return self.data?.shareButton ?? ""
    }
    
    var qrCodeInvalid: String {
        return self.data?.qrCodeInvalid ?? ""
    }
    
    var youAlreadyHaveThatNote: String {
        return self.data?.youAlreadyHaveThatNote ?? ""
    }
    
    var longPressNoteToSeeOptions: String {
        return self.data?.longPressNoteToSeeOptions ?? ""
    }
    
    var shareNote: String {
        return self.data?.shareNote ?? ""
    }
    
    var noNotesYet: String {
        return self.data?.noNotesYet ?? ""
    }
    
    var noteCreatedSuccessfully: String {
        return self.data?.noteCreatedSuccessfully ?? ""
    }
    
    var noteEditSuccessfully: String {
        return self.data?.noteEditSuccessfully ?? ""
    }
    
    var logoutButton: String {
        return self.data?.logoutButton ?? ""
    }
    
    var emailTitle: String {
        return self.data?.emailTitle ?? ""
    }
    
    var myNotes: String {
        return self.data?.myNotes ?? ""
    }
    
    var enterNoteContent: String {
        return self.data?.enterNoteContent ?? ""
    }
    
    var enterNoteNamePlaceholder: String {
        return self.data?.enterNoteNamePlaceholder ?? ""
    }
    
    var enterNoteName: String {
        return self.data?.enterNoteName ?? ""
    }
    
    var saveButton: String {
        return self.data?.saveButton ?? ""
    }
    
    init() {
        self.changeLanguage(to: Utils.shared.getDeviceLanguage())
    }
    
    private func parseLanguage(lang: String) -> MNLanguage? {
        if let path = Bundle.main.path(forResource: lang, ofType: "plist") {
            do {
                let data: Data = try Data(contentsOf: URL(fileURLWithPath: path))
                var propertyListFormat =  PropertyListSerialization.PropertyListFormat.xml
                if let dict = try? PropertyListSerialization.propertyList(from: data, options: [], format: &propertyListFormat) as? [String: String]  {
                    self.plistDict = dict
                }
                let language: MNLanguage = try PropertyListDecoder().decode(MNLanguage.self, from: data)
                return language
            } catch {
                return nil
            }
        }
        return nil
    }
    
    func changeLanguage(to lang: String) {
        var languageToChangeTo: String = lang
        if !Utils.shared.availableLanguages.contains(languageToChangeTo) {
            languageToChangeTo = AppConstants.defaultLanguage
        }
        self.data = self.parseLanguage(lang: languageToChangeTo)
    }
    
    func getTranslations(from key: String) -> String {
        return self.plistDict[key] ?? self.genericError
    }
    
}
