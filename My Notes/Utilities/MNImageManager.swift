//
//  MNImageManager.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import UIKit

class MNImageManager {
    
    static let shared: MNImageManager = MNImageManager()
    
    func encodeImageToBase64(_ image: UIImage) -> String
    {
        let imageString = "data:image/jpeg;base64," + (image.jpegData(compressionQuality: 0.5)?.base64EncodedString(options: .lineLength64Characters))!
        return imageString
    }
    
    func decodeBase64Image(_ imageString:String) -> UIImage?
    {
        let newString = imageString.replacingOccurrences(of: "data:image/jpeg;base64,", with: "")
        if let data = Data(base64Encoded: newString, options: .ignoreUnknownCharacters) {
            return UIImage(data: data)
        }
        return nil
    }
    
}
