//
//  MyNotesAppViewModel.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

class MNEnviromentCoordinator: ObservableObject {
    
    @Published var mainView: MainScreenEntryPoint = .login
    @Published var isLoading: Bool = false
    @Published var hasInternetConnection: Bool = true
    let persistenceController = PersistenceController.shared
    var isFirstTime: Bool = true
    private let networkManager: MNNetworkManager = MNNetworkManager()
    
    init() {
        self.networkManager.callback = { result in
            self.hasInternetConnection = result
            NotificationCenter.default.post(name: .internetConnectivityChanged, object: nil)
            if result {
                self.hideNoInternetView()
            }else {
                MNNotificationsManager.shared.showNoInternetBanner(message: MNLanguageManager.shared.noInternet, type: .danger)
            }
            self.isFirstTime = false
        }
        self.checkWichPageToShow()
    }
    
    func showAlert(message: String, type: MNServerMessageType) {
        MNNotificationsManager.shared.showAlert(message: message, style: type.banner)
    }
    
    func logout() {
        guard self.networkManager.hasInternetConnection() else {
            self.showAlert(message: MNLanguageManager.shared.noInternet, type: .error)
            return
        }
        MNStorageManager.shared.saveUserToken(token: "")
        APIClient.shared.headers.token = ""
        self.persistenceController.deleteAll()
        self.mainView = .login
    }
    
    private func checkWichPageToShow() {
        if MNStorageManager.shared.getUserToken().isEmpty {
            self.mainView = .login
        }else {
            self.mainView = .notes
        }
    }
    
    private func hideNoInternetView() {
        if !self.isFirstTime {
            MNNotificationsManager.shared.hideNoInternetBanner()
            MNNotificationsManager.shared.showNoInternetBanner(message: MNLanguageManager.shared.internetRestored, type: .success)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            MNNotificationsManager.shared.hideNoInternetBanner()
        }
    }
    
}
