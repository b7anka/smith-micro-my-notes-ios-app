//
//  Structs.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct AppConstants {
    
    static let prodBundleIdentifier: String = "com.tiago-moreira.My-Notes"
    static let testBundleIdentifier: String = "com.tiago-moreira.My-Notes-test"
    static let bundleIdentifierKey: String = "CFBundleIdentifier"
    static let defaultLanguage: String = "en"
    static let defaultURLRequestTimeout: CGFloat = 5.0
    static let userInvalidKey: String = "user_invalid"
    
}

struct ImageAssets {
    
    static let noteDefaultImage: String = "note.text"
    static let userDefaultImage: String = "person.fill"
    static let mailImage: String = "mail.fill"
    static let passwordImage: String = "lock.fill"
    static let logoutImage: String = "lock.open.fill"
    static let noInternetImage: String = "wifi.slash"
    static let internetRestoredImage: String = "wifi"
    static let registerImage: String = "person.fill.badge.plus"
    static let closeImage: String = "xmark"
    static let chooseCameraImage: String = "arrow.triangle.2.circlepath.camera"
    static let cameraImage: String = "camera"
    static let chooseFromLibraryImage: String = "photo.fill.on.rectangle.fill"
    static let plusImage: String = "plus"
    static let shareImage: String = "square.and.arrow.up.fill"
    static let deleteImage: String = "trash.fill"
    static let editImage: String = "pencil"
    static let newNoteImage: String = "note.text.badge.plus"
    static let saveImage: String = "square.and.arrow.down.fill"
    static let qrCodeImage: String = "qrcode.viewfinder"
    
}

struct UserDefaultsKeys {
    
    static let userToken: String = "token"
    
}

struct AccessibilityIdentifiers {
    
    static let loginImage: String = "loginImage"
    static let loginEmailLabel: String = "loginEmailLabel"
    static let loginEmailTextField: String = "loginEmailTextField"
    
    static let loginPasswordLabel: String = "loginEmailLabel"
    static let loginPasswordTextField: String = "loginPasswordTextField"
    
    static let loginButton: String = "loginButton"
    static let loginRegisterButton: String = "loginRegisterButton"
    
    static let registerImageSelectionButton: String = "registerImageSelectionButton"
    static let registerImageSelectionLabel: String = "registerImageSelectionLabel"
    static let registerEmailLabel: String = "registerEmailLabel"
    static let registerEmailTextField: String = "registerEmailTextField"
    
    static let registerPasswordLabel: String = "registerPasswordLabel"
    static let registerPasswordTextField: String = "registerPasswordTextField"
    
    static let registerConfirmPasswordLabel: String = "registerConfirmPasswordLabel"
    static let registerConfirmPasswordTextField: String = "registerConfirmPasswordTextField"
    
    static let registerButton: String = "registerButton"
    static let chooseFromLibraryRegisterButton: String = "chooseFromLibraryRegisterButton"
    static let chooseCameraRegisterButton: String = "chooseCameraRegisterButton"
    
}
