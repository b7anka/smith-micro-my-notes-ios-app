//
//  StorageManager.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

class MNStorageManager {
    
    static let shared: MNStorageManager = MNStorageManager()
    private let defaults: UserDefaults = UserDefaults.standard
    
    // MARK: - SAVING
    func saveUserToken(token: String) {
        self.defaults.setValue(token, forKey: UserDefaultsKeys.userToken)
    }
    
    // MARK: RETRIEVING
    func getUserToken() -> String {
        return self.defaults.string(forKey: UserDefaultsKeys.userToken) ?? ""
    }
    
}
