//
//  MNAlertManager.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import Foundation
import NotificationBannerSwift

class MNNotificationsManager {
    
    static let shared: MNNotificationsManager = MNNotificationsManager()
    var noInternetBanner: NotificationBanner?
    private let bannerQueueToDisplaySeveralBanners = NotificationBannerQueue(maxBannersOnScreenSimultaneously: 2)
    private var isShowingBanners: Bool = false
    private var isShowingNoInternetBanner: Bool = false
    
    func showAlert(message: String, style: BannerStyle) {
        guard !self.isShowingBanners else {return}
        let banner = NotificationBanner(title: message, subtitle: "", leftView: nil, rightView: nil, style: style, colors: nil)
        banner.delegate = self
        self.isShowingBanners = true
        banner.show(queuePosition: QueuePosition.front, bannerPosition: .top, queue: self.bannerQueueToDisplaySeveralBanners, on: nil)
    }
    
    func showNoInternetBanner(message: String, type: BannerStyle) {
        guard !self.isShowingNoInternetBanner else {return}
        self.noInternetBanner = NotificationBanner(title: message, style: type)
        self.noInternetBanner?.autoDismiss = false
        self.noInternetBanner?.show(queuePosition: QueuePosition.front, bannerPosition: .bottom, queue: self.bannerQueueToDisplaySeveralBanners, on: nil)
        self.isShowingNoInternetBanner = true
    }
    
    func hideNoInternetBanner() {
        self.noInternetBanner?.dismiss()
        self.noInternetBanner = nil
        self.isShowingNoInternetBanner = false
    }
    
}

extension MNNotificationsManager: NotificationBannerDelegate {
    
    func notificationBannerWillAppear(_ banner: BaseNotificationBanner) {
        //not needed
    }
    
    func notificationBannerDidAppear(_ banner: BaseNotificationBanner) {
        //not needed
    }
    
    func notificationBannerWillDisappear(_ banner: BaseNotificationBanner) {
        //not needed
    }
    
    func notificationBannerDidDisappear(_ banner: BaseNotificationBanner) {
        self.isShowingBanners = false
    }
    
}
