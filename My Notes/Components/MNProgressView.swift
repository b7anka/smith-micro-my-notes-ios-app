//
//  MNProgressView.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct MNProgressView: View {
    
    var body: some View {
        VStack {
            Spacer()
            ProgressView().progressViewStyle(CircularProgressViewStyle())
            Spacer()
        }
    }
    
}
