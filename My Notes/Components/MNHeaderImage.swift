//
//  MNHeaderImage.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct MNHeaderImage: View {
    
    var systemImageName: String
    var identifier: String
    
    var body: some View {
        Image(systemName: self.systemImageName).resizable().frame(width: 60, height: 60).padding(.bottom, 50).accessibilityIdentifier(self.identifier)
    }
    
}
