//
//  MNCloseButton.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct MNCloseButton: View {
    
    var action: (() -> Void)
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            VStack {
                Image(systemName: ImageAssets.closeImage).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20).foregroundColor(.init(UIColor.label))
            }.frame(width: 50, height: 50)
        }.padding(.leading).padding(.top, 30)
    }
    
}
