//
//  MNTextEditor.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI

struct MNTextEditor: View {
    
    var labelTitle: String
    var labelSystemImage: String
    var keyboardType: UIKeyboardType
    var textContentType: UITextContentType
    @Binding var text: String
    
    var body: some View {
        VStack {
            HStack {
                Label(self.labelTitle, systemImage: ImageAssets.mailImage)
                Spacer()
            }.padding(.top)
            TextEditor(text: self.$text).textContentType(self.textContentType).keyboardType(self.keyboardType).frame(height: UIScreen.main.bounds.height * 0.4).border(Color.init(UIColor.label), width: 2)
            Spacer()
        }.padding(.horizontal)
    }
    
}

