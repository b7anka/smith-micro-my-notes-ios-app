//
//  MNCameraPreview.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI
import AVFoundation

struct MNCameraPreview: UIViewRepresentable {
    
    @StateObject var cameraViewModel: CameraViewModel
    
    func makeUIView(context: Context) -> UIView {
        let view: UIView = UIView(frame: UIScreen.main.bounds)
        self.cameraViewModel.preview  = AVCaptureVideoPreviewLayer(session: self.cameraViewModel.session)
        self.cameraViewModel.preview.frame = view.frame
        self.cameraViewModel.preview.videoGravity = .resizeAspectFill
        view.layer.addSublayer(self.cameraViewModel.preview)
        self.cameraViewModel.session.startRunning()
        return view
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
    
}
