//
//  MNTextFieldWithLabel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct MNTextFieldWithLabel: View {
    
    var labelTitle: String
    var labelSystemImage: String
    var placeHolder: String
    var keyboardType: UIKeyboardType
    var textContentType: UITextContentType
    @Binding var text: String
    var labelIdentifier: String
    var textFieldIdentifier: String
    
    var body: some View {
        VStack {
            HStack {
                Label(self.labelTitle, systemImage: ImageAssets.mailImage).accessibilityIdentifier(self.labelIdentifier)
                Spacer()
            }
            TextField(self.placeHolder, text: self.$text).textContentType(self.textContentType).keyboardType(self.keyboardType).accessibilityIdentifier(self.textFieldIdentifier)
        }.padding(.horizontal)
    }
    
}
