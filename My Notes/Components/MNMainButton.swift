//
//  MNMainButton.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct MNMainButton: View {
    
    var title: String
    var systemImage: String
    var action: (() -> Void)
    @Binding var enabled: Bool
    var identifier: String
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            Label(self.title, systemImage: self.systemImage).foregroundColor(.white).lineLimit(1).minimumScaleFactor(0.5).padding(.horizontal)
        }.disabled(!self.enabled)
        .frame(width: UIScreen.main.bounds.width * 0.3, height: 50).background(Color.blue).cornerRadius(10)
        .opacity(!self.enabled ? 0.5 : 1.0).shadow(color: .gray, radius: 5, x: 0, y: 0)
        .accessibilityIdentifier(self.identifier)
    }
    
}
