//
//  MNContextMenuButton.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct MNContextMenuButton: View {
    
    var labelText: String
    var imageName: String
    var action: (() -> Void)
    
    var body: some View {
        Button(action: self.action) {
            HStack {
                Image(systemName: self.imageName).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20)
                Text(self.labelText)
            }
        }
    }
    
}
