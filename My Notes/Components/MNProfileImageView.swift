//
//  MNProfileImageView.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI

struct MNProfileImageView: View {
    
    @Binding var image: UIImage?
    
    var body: some View {
        VStack {
            if let image = self.image {
                Image(uiImage: image).resizable().aspectRatio(contentMode: .fill).frame(width: 100, height: 100).padding(.bottom, 50).clipShape(Circle()).overlay(
                    Circle().stroke(Color.init(UIColor.label), lineWidth: 2)
                ).shadow(color: Color.init(UIColor.label), radius: 6, x: 0, y: 0)
            }else {
                Image(systemName: ImageAssets.userDefaultImage).resizable().aspectRatio(contentMode: .fill).frame(width: 60, height: 60).padding(.bottom, 50).foregroundColor(Color.init(UIColor.label))
            }
        }.padding(.top, 30)
    }
}

