//
//  MNImagePickerView.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct MNImagePickerView: UIViewControllerRepresentable {
    
    typealias UIViewControllerType = UIImagePickerController
    typealias SourceType = UIImagePickerController.SourceType

    let sourceType: SourceType
    let completionHandler: (UIImage?) -> Void
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let viewController = UIImagePickerController()
        viewController.delegate = context.coordinator
        viewController.sourceType = sourceType
        return viewController
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}
    
    func makeCoordinator() -> MNImagePickerViewCoordinator {
        return Coordinator(completionHandler: self.completionHandler)
    }
    
}

final class MNImagePickerViewCoordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let completionHandler: (UIImage?) -> Void
    
    init(completionHandler: @escaping (UIImage?) -> Void) {
        self.completionHandler = completionHandler
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let image: UIImage? = {
            if let image = info[.editedImage] as? UIImage {
                return image
            }
            return info[.originalImage] as? UIImage
        }()
        self.completionHandler(image)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.completionHandler(nil)
    }
    
}
