//
//  MNUserAvatarButton.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct MNUserAvatarButton: View {
    
    var labelTitle: String
    var image: UIImage?
    var systemImage: String
    var action: (() -> Void)
    var buttonIdentifier: String
    var labelIdentifier: String
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            VStack {
                if let image = self.image {
                    Image(uiImage: image).resizable().aspectRatio(contentMode: .fill).frame(width: 100, height: 100).padding(.bottom, 50).clipShape(Circle()).overlay(
                        Circle().stroke(Color.init(UIColor.label), lineWidth: 2)
                    ).shadow(color: Color.init(UIColor.label), radius: 6, x: 0, y: 0)
                }else {
                    Image(systemName: self.systemImage).resizable().aspectRatio(contentMode: .fill).frame(width: 60, height: 60).padding(.bottom, 50).foregroundColor(Color.init(UIColor.label))
                }
                Text(self.labelTitle).fontWeight(.semibold).lineLimit(2).minimumScaleFactor(0.6).foregroundColor(Color.init(UIColor.label)).accessibilityIdentifier(self.labelIdentifier)
            }.padding(.bottom)
        }.accessibilityIdentifier(self.buttonIdentifier)
    }
    
}
