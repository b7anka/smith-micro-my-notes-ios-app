//
//  MNSecureFieldWithLabel.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct MNSecureFieldWithLabel: View {
    
    var labelTitle: String
    var systemImageName: String
    var placeHolder: String
    var textContentType: UITextContentType
    @Binding var text: String
    var labelIdentifier: String
    var textFieldIdentifier: String
    
    var body: some View {
        VStack {
            HStack {
                Label(self.labelTitle, systemImage: self.systemImageName).accessibilityIdentifier(self.labelIdentifier)
                Spacer()
            }
            SecureField(self.placeHolder, text: self.$text).textContentType(self.textContentType).accessibilityIdentifier(self.textFieldIdentifier)
        }.padding(.horizontal)
    }
    
}
