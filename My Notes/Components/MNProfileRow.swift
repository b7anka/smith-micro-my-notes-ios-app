//
//  MNProfileRow.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI

struct MNProfileRow: View {
    
    @Binding var email: String
    
    var body: some View {
        HStack {
            Spacer()
            Text(MNLanguageManager.shared.emailTitle)
            Spacer()
            Text(self.email)
            Spacer()
        }.padding(.horizontal).padding(.top)
    }
    
}
