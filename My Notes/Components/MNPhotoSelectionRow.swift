//
//  MNPhotoSelectionRow.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct MNPhotoSelectionRow: View {
    
    var libraryButtonAction: (() -> Void)
    var cameraButtonAction: (() -> Void)
    var cameraIdentifier: String
    var libraryIdentifier: String
    
    var body: some View {
        VStack {
            Spacer()
            HStack {
                MNCameraPhotoSelectionButton(systemImage: ImageAssets.chooseFromLibraryImage, action: self.libraryButtonAction, identifier: self.libraryIdentifier)
                Spacer()
                MNCameraPhotoSelectionButton(systemImage: ImageAssets.cameraImage, action: self.cameraButtonAction, identifier: self.cameraIdentifier)
            }.padding(.bottom).padding(.horizontal)
        }
    }
    
}
