//
//  MNCameraPhotoSelectionButton.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct MNCameraPhotoSelectionButton: View {
    
    var systemImage: String
    var action: (() -> Void)
    var identifier: String
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            VStack {
                ZStack {
                    Circle().fill(Color.white).frame(width: 45, height: 45)
                    Image(systemName: self.systemImage).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20).foregroundColor(Color.black)
                }
            }.frame(width: 50, height: 50)
        }.accessibilityIdentifier(self.identifier)
    }
    
}

