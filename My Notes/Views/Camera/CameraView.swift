//
//  CameraView.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct CameraView: View {
    
    @StateObject var cameraViewModel: CameraViewModel = CameraViewModel()
    @Environment(\.presentationMode) var presentationMode
    var completion: ((String) -> Void)
    
    var body: some View {
        ZStack {
            MNCameraPreview(cameraViewModel: self.cameraViewModel)
        }.onAppear {
            self.cameraViewModel.completion = self.completion
            self.cameraViewModel.checkPermission()
        }.edgesIgnoringSafeArea(.all)
    }
    
}
