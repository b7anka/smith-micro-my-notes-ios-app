//
//  RegisterView.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct RegisterView: View {
    
    @EnvironmentObject var screenCoordinator: MNEnviromentCoordinator
    @StateObject var registerViewModel: RegisterViewModel = RegisterViewModel()
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            VStack {
                ScrollView(.vertical, showsIndicators: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, content: {
                    MNUserAvatarButton(labelTitle: MNLanguageManager.shared.tapToChooseImage, image: self.registerViewModel.image, systemImage: ImageAssets.registerImage, action: {self.registerViewModel.showButtonToPickImage.toggle()}, buttonIdentifier: AccessibilityIdentifiers.registerImageSelectionButton, labelIdentifier: AccessibilityIdentifiers.registerImageSelectionLabel)
                    MNTextFieldWithLabel(labelTitle: MNLanguageManager.shared.enterYourEmail, labelSystemImage: ImageAssets.mailImage, placeHolder: MNLanguageManager.shared.emailPlaceholder, keyboardType: .emailAddress, textContentType: .emailAddress, text: self.$registerViewModel.emailText, labelIdentifier: AccessibilityIdentifiers.registerEmailLabel, textFieldIdentifier: AccessibilityIdentifiers.registerEmailTextField)
                    MNSecureFieldWithLabel(labelTitle: MNLanguageManager.shared.enterYourPassword, systemImageName: ImageAssets.passwordImage, placeHolder: MNLanguageManager.shared.passwordPlaceholder, textContentType: .password, text: self.$registerViewModel.passwordText, labelIdentifier: AccessibilityIdentifiers.registerPasswordLabel, textFieldIdentifier: AccessibilityIdentifiers.registerPasswordTextField)
                    MNSecureFieldWithLabel(labelTitle: MNLanguageManager.shared.confirmPassword, systemImageName: ImageAssets.passwordImage, placeHolder: MNLanguageManager.shared.passwordPlaceholder, textContentType: .password, text: self.$registerViewModel.confirmPasswordText, labelIdentifier: AccessibilityIdentifiers.registerConfirmPasswordLabel, textFieldIdentifier: AccessibilityIdentifiers.registerConfirmPasswordTextField)
                    Spacer()
                    MNMainButton(title: MNLanguageManager.shared.registerButton, systemImage: ImageAssets.registerImage, action: {
                        self.hideKeyboard()
                        self.registerViewModel.register()
                    }, enabled: self.$registerViewModel.shouldEnableRegisterButton, identifier: AccessibilityIdentifiers.registerButton)
                    Spacer()
                })
            }.disabled(self.screenCoordinator.isLoading)
            .navigationTitle(MNLanguageManager.shared.registerButton)
            if self.registerViewModel.showButtonToPickImage {
                MNPhotoSelectionRow(libraryButtonAction: self.registerViewModel.showLibrary, cameraButtonAction: self.registerViewModel.showCamera, cameraIdentifier: AccessibilityIdentifiers.chooseCameraRegisterButton, libraryIdentifier: AccessibilityIdentifiers.chooseFromLibraryRegisterButton)
            }
            if self.screenCoordinator.isLoading {
                MNProgressView()
            }
        }.onAppear {
            self.registerViewModel.coordinator = self.screenCoordinator
            self.registerViewModel.presentationMode = self.presentationMode
        }.fullScreenCover(isPresented: self.$registerViewModel.showImagePickerView) {
            MNImagePickerView(sourceType: self.registerViewModel.sourceType, completionHandler: self.registerViewModel.didSelectImage)
        }
    }
    
}
