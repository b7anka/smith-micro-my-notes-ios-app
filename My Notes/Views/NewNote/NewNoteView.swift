//
//  NewNoteView.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI

struct NewNoteView: View {
    
    @StateObject var newNoteViewModel: NewNoteViewModel = NewNoteViewModel()
    @EnvironmentObject var coordinator: MNEnviromentCoordinator
    @Environment(\.presentationMode) var presentationMode
    var note: MNNoteViewModel?
    var callback: ((MNNoteViewModel) -> Void)
    
    var body: some View {
        ZStack {
            ScrollView(.vertical, showsIndicators: false, content: {
                VStack {
                    Spacer()
                    MNUserAvatarButton(labelTitle: MNLanguageManager.shared.tapToChooseImage, image: self.newNoteViewModel.noteImage, systemImage: ImageAssets.newNoteImage, action: {
                        self.hideKeyboard()
                        self.newNoteViewModel.showPickerButtons()
                    }, buttonIdentifier: "", labelIdentifier: "")
                    MNTextFieldWithLabel(labelTitle: MNLanguageManager.shared.enterNoteName, labelSystemImage: "", placeHolder: MNLanguageManager.shared.enterNoteNamePlaceholder, keyboardType: .default, textContentType: .name, text: self.$newNoteViewModel.noteNameText, labelIdentifier: "", textFieldIdentifier: "")
                    MNTextEditor(labelTitle: MNLanguageManager.shared.enterNoteContent, labelSystemImage: "", keyboardType: .default, textContentType: .name, text: self.$newNoteViewModel.noteContentText)
                    MNMainButton(title: MNLanguageManager.shared.saveButton, systemImage: ImageAssets.saveImage, action: {
                        self.hideKeyboard()
                        self.newNoteViewModel.saveNote()
                    }, enabled: self.$newNoteViewModel.enableSaveButton, identifier: "")
                    .padding(.top, 30)
                }.disabled(self.coordinator.isLoading)
                .padding(.bottom, 50)
                if self.newNoteViewModel.showImagePickerButtons {
                    MNPhotoSelectionRow(libraryButtonAction: self.newNoteViewModel.showLibrary, cameraButtonAction: self.newNoteViewModel.showCamera, cameraIdentifier: "", libraryIdentifier: "")
                }
                if self.coordinator.isLoading {
                    MNProgressView()
                }
            })
        }.onAppear {
            self.newNoteViewModel.coordinator = self.coordinator
            self.newNoteViewModel.note = self.note
            self.newNoteViewModel.presentationMode = self.presentationMode
            self.newNoteViewModel.callback = self.callback
        }.fullScreenCover(isPresented: self.$newNoteViewModel.presentImagePickerView, content: {
            MNImagePickerView(sourceType: self.newNoteViewModel.sourceType, completionHandler: self.newNoteViewModel.didSelectImage)
        })
    }
    
}
