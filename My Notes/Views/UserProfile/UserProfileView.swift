//
//  UserProfileView.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import SwiftUI

struct UserProfileView: View {
    
    @StateObject var userProfileViewModel: UserProfileViewModel = UserProfileViewModel()
    @EnvironmentObject var coordinator: MNEnviromentCoordinator
    
    var body: some View {
        VStack {
            MNProfileImageView(image: self.$userProfileViewModel.userImage)
            MNProfileRow(email: self.$userProfileViewModel.userEmail)
            MNMainButton(title: MNLanguageManager.shared.logoutButton, systemImage: ImageAssets.passwordImage, action: self.userProfileViewModel.logout, enabled: self.$userProfileViewModel.logoutButtonEnabled, identifier: "").padding(.top, 30)
            Spacer()
        }.onAppear {
            self.userProfileViewModel.coordinator = self.coordinator
            self.userProfileViewModel.getUserFromRealm()
        }
    }
    
}
