//
//  NotesView.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct NotesView: View {
    
    @StateObject var notesViewModel: NotesViewModel = NotesViewModel()
    @EnvironmentObject var screenCoordinator: MNEnviromentCoordinator
    
    var body: some View {
        VStack {
            NoteListView(notesCollection: self.notesViewModel.notesCollection, notesViewModel: self.notesViewModel)
        }.toolbar {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: self.notesViewModel.showUserProfile) {
                    Image(systemName: ImageAssets.userDefaultImage).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20).foregroundColor(Color.init(UIColor.label))
                }
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: self.notesViewModel.showCameraPreview) {
                    Image(systemName: ImageAssets.qrCodeImage).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20).foregroundColor(Color.init(UIColor.label))
                }
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: self.notesViewModel.createNote) {
                    Image(systemName: ImageAssets.plusImage).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20).foregroundColor(Color.init(UIColor.label))
                }
            }
        }.sheet(item: self.$notesViewModel.pageToShow) { _ in
            switch self.notesViewModel.pageToShow {
            case .newNote:
                NewNoteView(note: self.notesViewModel.noteToEdit, callback: self.notesViewModel.didEditNote).environmentObject(self.screenCoordinator)
            case .cameraView:
                CameraView(completion: self.notesViewModel.didAddNoteFromQRCode)
            default:
                UserProfileView().environmentObject(self.screenCoordinator)
            }
        }.padding(.top)
        .onAppear {
            self.notesViewModel.screenCoordinator = self.screenCoordinator
        }.navigationTitle(MNLanguageManager.shared.myNotes)
    }
    
}
