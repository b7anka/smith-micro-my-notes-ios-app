//
//  NoteListView.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct NoteListView: View {
    
    let notesCollection: [MNNoteViewModel]
    let notesViewModel: NotesViewModel
    
    var body: some View {
        if self.notesCollection.isEmpty {
            VStack {
                MNHeaderImage(systemImageName: ImageAssets.noteDefaultImage, identifier: "")
                Text(MNLanguageManager.shared.noNotesYet).fontWeight(.semibold)
                    .lineLimit(2).minimumScaleFactor(0.7)
                Spacer()
            }
        } else {
            VStack {
                Text(MNLanguageManager.shared.longPressNoteToSeeOptions).fontWeight(.semibold)
                    .lineLimit(2).minimumScaleFactor(0.7)
                ScrollView(.vertical, showsIndicators: false) {
                    ForEach(self.notesCollection, id: \.id) { note in
                        NoteListCell(note: note, notesViewModel: self.notesViewModel)
                    }
                }
            }
        }
    }
}

