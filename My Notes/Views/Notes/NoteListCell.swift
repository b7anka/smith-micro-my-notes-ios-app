//
//  NoteListCell.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct NoteListCell: View {
    
    let note: MNNoteViewModel
    var notesViewModel: NotesViewModel
    
    var body: some View {
        HStack {
            if let image = MNImageManager.shared.decodeBase64Image(self.note.imageBase64) {
                Image(uiImage: image).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20)
                
            }else {
                Image(systemName: ImageAssets.noteDefaultImage).resizable().aspectRatio(contentMode: .fill).frame(width: 20, height: 20)
            }
            Spacer()
            Text(self.note.name).fontWeight(.semibold).lineLimit(1).minimumScaleFactor(0.7)
            Spacer()
            Text(self.note.date).fontWeight(.semibold).lineLimit(1).minimumScaleFactor(0.5)
        }.frame(width: UIScreen.main.bounds.width * 0.9, height: 40)
        .contextMenu {
            MNContextMenuButton(labelText: MNLanguageManager.shared.editButton, imageName: ImageAssets.editImage, action: {self.notesViewModel.editNote(note)})
            if let user = self.notesViewModel.user, Int(note.ownerId) == user.id {
                MNContextMenuButton(labelText: MNLanguageManager.shared.shareButton,imageName: ImageAssets.shareImage, action: {self.notesViewModel.shareNote(note)})
                MNContextMenuButton(labelText: MNLanguageManager.shared.deleteButton,imageName: ImageAssets.deleteImage, action: {self.notesViewModel.deleteNote(note)})
            }
        }.onTapGesture {
            self.notesViewModel.editNote(note)
        }
    }
}
