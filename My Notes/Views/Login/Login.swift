//
//  Login.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import SwiftUI

struct Login: View {
    
    @EnvironmentObject var screenCoordinator: MNEnviromentCoordinator
    @StateObject var loginViewModel: LoginViewModel = LoginViewModel()
    
    var body: some View {
        ZStack {
            VStack {
                MNHeaderImage(systemImageName: ImageAssets.noteDefaultImage, identifier: AccessibilityIdentifiers.loginImage)
                MNTextFieldWithLabel(labelTitle: MNLanguageManager.shared.enterYourEmail, labelSystemImage: ImageAssets.mailImage, placeHolder: MNLanguageManager.shared.emailPlaceholder, keyboardType: .emailAddress, textContentType: .emailAddress, text: self.$loginViewModel.emailText, labelIdentifier: AccessibilityIdentifiers.loginEmailLabel, textFieldIdentifier: AccessibilityIdentifiers.loginEmailTextField)
                MNSecureFieldWithLabel(labelTitle: MNLanguageManager.shared.enterYourPassword, systemImageName: ImageAssets.passwordImage, placeHolder: MNLanguageManager.shared.passwordPlaceholder, textContentType: .password, text: self.$loginViewModel.passwordText, labelIdentifier: AccessibilityIdentifiers.loginPasswordLabel, textFieldIdentifier: AccessibilityIdentifiers.loginPasswordTextField)
                Spacer()
                HStack {
                    Spacer()
                    MNMainButton(title: MNLanguageManager.shared.loginButton, systemImage: ImageAssets.logoutImage, action: {
                        self.hideKeyboard()
                        self.loginViewModel.login()
                    }, enabled: self.$loginViewModel.enabledButton, identifier: AccessibilityIdentifiers.loginButton)
                    Spacer()
                    MNMainButton(title: MNLanguageManager.shared.registerButton, systemImage: ImageAssets.registerImage, action: {
                        self.hideKeyboard()
                        self.loginViewModel.register()
                    }, enabled: self.$loginViewModel.enabledRegisterButton, identifier: AccessibilityIdentifiers.loginRegisterButton)
                    Spacer()
                }
                Spacer()
            }.disabled(self.screenCoordinator.isLoading)
            NavigationLink(destination: RegisterView(), tag: LoginViewNavigation.register, selection: self.$loginViewModel.pageToShow){}.hidden()
        }.onAppear {
            self.loginViewModel.screenCoordinator = self.screenCoordinator
        }
    }
    
}
