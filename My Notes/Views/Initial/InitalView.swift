//
//  InitalView.swift
//  My Notes
//
//  Created by João Moreira on 27/03/2021.
//

import SwiftUI

struct InitalView: View {
    
    @EnvironmentObject var screenCoordinator: MNEnviromentCoordinator
    
    var body: some View {
        ZStack {
            switch self.screenCoordinator.mainView {
            case .login:
                Login()
            case .notes:
                NotesView()
            }
            if self.screenCoordinator.isLoading {
                MNProgressView()
            }
        }.navigationViewStyle(DoubleColumnNavigationViewStyle())
    }
    
}
