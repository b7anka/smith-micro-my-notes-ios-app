//
//  MNServerMessage.swift
//  My Notes
//
//  Created by João Moreira on 25/03/2021.
//

import Foundation

struct MNServerMessage: Decodable {
    
    let message: String?
    let type: String?
    let success: Bool?
    let timestamp: Int64?
    let status: Int?
    let error: String?
    let exception: String?
    let path: String?
    
}
