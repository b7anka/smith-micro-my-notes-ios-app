//
//  MNHeaders.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNHeaders: Encodable {
    
    let apiKey: String
    var token: String
    
    init(apiKey: String, token: String) {
        self.apiKey = apiKey
        self.token = token
    }
    
    func toDictionary() -> [String: String] {
        do {
            let data: Data = try JSONEncoder().encode(self)
            if let dict: [String: String] = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: String] {
                return dict
            }
            return [:]
        } catch {
            return [:]
        }
    }
    
}
