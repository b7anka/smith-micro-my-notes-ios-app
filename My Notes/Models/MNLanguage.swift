//
//  MNLanguage.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNLanguage: Decodable {
    
    let apiKeyMissing: String
    let emailOrPasswordWrong: String
    let userDoesNotExtist: String
    let userAlreadyExists: String
    let noPermissionToEditNote: String
    let noteDeletedSuccessfully: String
    let cantDeleteNote: String
    let userInvalid: String
    let deleteButton: String
    let logoutSuccessfully: String
    let logoutFailed: String
    let registeredSuccessfully: String
    let genericError: String
    let loginButton: String
    let enterYourEmail: String
    let enterYourPassword: String
    let emailPlaceholder: String
    let passwordPlaceholder: String
    let registerButton: String
    let noInternet: String
    let internetRestored: String
    let confirmPassword: String
    let emailInvalid: String
    let passwordsDontMatch: String
    let libraryPermissionNotGiven: String
    let cameraPermissionNotGiven: String
    let tapToChooseImage: String
    let editButton: String
    let shareButton: String
    let qrCodeInvalid: String
    let youAlreadyHaveThatNote: String
    let longPressNoteToSeeOptions: String
    let shareNote: String
    let noNotesYet: String
    let noteCreatedSuccessfully: String
    let noteEditSuccessfully: String
    let logoutButton: String
    let emailTitle: String
    let myNotes: String
    let enterNoteName: String
    let enterNoteNamePlaceholder: String
    let enterNoteContent: String
    let saveButton: String
    
    private enum CodingKeys: String, CodingKey {
        case apiKeyMissing = "api_key_or_token_missing_or_wrong", emailOrPasswordWrong = "email_or_password_dont_match", userDoesNotExtist = "user_does_not_exist", userAlreadyExists = "user_already_exists", noPermissionToEditNote = "no_permission_edit_note", noteDeletedSuccessfully = "note_deleted_success", cantDeleteNote = "cannot_delete_note", userInvalid = "user_invalid", deleteButton = "delete_button", logoutSuccessfully = "logout_success", logoutFailed = "logout_failed", registeredSuccessfully = "registered_successfully", genericError = "generic_error", loginButton = "login_button", enterYourEmail = "enter_your_email", enterYourPassword = "enter_your_password", emailPlaceholder = "email_placeholder", passwordPlaceholder = "password_placeholder", registerButton = "register_button",
             noInternet = "no_internet", internetRestored = "internet_restored", confirmPassword = "comfirm_password", emailInvalid = "email_invalid", passwordsDontMatch = "passwords_dont_match", libraryPermissionNotGiven = "library_permission_not_given", cameraPermissionNotGiven = "camera_permission_not_given", tapToChooseImage = "tap_to_choose_an_image", editButton = "edit_button", shareButton = "share_button", qrCodeInvalid = "qr_code_invalid", youAlreadyHaveThatNote = "you_already_have_that_note", longPressNoteToSeeOptions = "long_press_note_to_see_options", shareNote = "share_note", noNotesYet = "no_notes_yet", noteCreatedSuccessfully = "note_created_successfully",  noteEditSuccessfully = "note_edit_successfully", logoutButton = "logout_button", emailTitle = "email_title", myNotes = "my_notes", enterNoteName = "enter_note_name", enterNoteNamePlaceholder = "enter_note_name_placeholder", enterNoteContent = "enter_note_content", saveButton = "save_button"
    }
    
}
