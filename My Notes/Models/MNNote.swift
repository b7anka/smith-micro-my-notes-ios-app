//
//  MNNote.swift
//  My Notes
//
//  Created by João Moreira on 25/03/2021.
//

import Foundation

struct MNNote: Decodable, Encodable {
    
    var id: Int?
    var name: String?
    var date: String?
    var ownerId: String?
    var imageBase64: String?
    var content: String?
    
    func toDictionary() -> [String: Any] {
        do {
            let data = try JSONEncoder().encode(self)
            if let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                return dict
            }
            return [:]
        } catch {
            return [:]
        }
    }
    
}

extension MNNote: Equatable {
    
    static func == (lhs: MNNote, rhs: MNNote) -> Bool {
        guard let lhsId = lhs.id, let rhsId = rhs.id else {return false}
        return lhsId == rhsId
    }
    
}
