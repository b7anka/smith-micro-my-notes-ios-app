//
//  MNAppConfig.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNAppConfig: Decodable {
    
    let apiKey: String
    let testServer: String
    let prodServer: String
    let qrCodeKey: String
    
}
