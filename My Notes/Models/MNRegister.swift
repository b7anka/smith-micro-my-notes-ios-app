//
//  MNRegister.swift
//  My Notes
//
//  Created by João Moreira on 25/03/2021.
//

import Foundation

struct MNRegister: Encodable {
    
    let basicData: MNLogin
    let imageBase64: String?
    
    init(basicData: MNLogin, imageBase64: String?) {
        self.basicData = basicData
        self.imageBase64 = imageBase64
    }
    
    func toDictionary() -> [String: Any] {
        do {
            let data = try JSONEncoder().encode(self)
            if let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                return dict
            }
            return [:]
        } catch {
            return [:]
        }
    }
    
}
