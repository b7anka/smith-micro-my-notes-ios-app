//
//  MNUser.swift
//  My Notes
//
//  Created by João Moreira on 25/03/2021.
//

import Foundation

struct MNUser: Decodable, Encodable {
    
    var id: Int?
    var email: String?
    var imageBase64: String?
    
    func toDictionary() -> [String: Any] {
        do {
            let data = try JSONEncoder().encode(self)
            if let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                return dict
            }
            return [:]
        } catch {
            return [:]
        }
    }
    
}

extension MNUser: Equatable {
    
    static func == (lhs: MNUser, rhs: MNUser) -> Bool {
        guard let lhsId = lhs.id, let rhsId = rhs.id else {return false}
        return lhsId == rhsId
    }
    
}
