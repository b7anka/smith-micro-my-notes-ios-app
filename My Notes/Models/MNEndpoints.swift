//
//  MNEndpoints.swift
//  My Notes
//
//  Created by João Moreira on 26/03/2021.
//

import Foundation

struct MNEndpoints: Decodable {
    
    let register: String
    let login: String
    let logout: String
    let users: String
    let note: String
    
}
