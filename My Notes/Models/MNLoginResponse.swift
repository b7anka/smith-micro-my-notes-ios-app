//
//  MNLoginResponse.swift
//  My Notes
//
//  Created by João Moreira on 25/03/2021.
//

import Foundation

struct MNLoginResponse: Decodable {
    
    let user: MNUser?
    let token: String?
    
}
