//
//  MNAddByQrCode.swift
//  My Notes
//
//  Created by João Moreira on 28/03/2021.
//

import Foundation

struct MNAddByQrCode: Encodable {
    
    let qrCodeData: String
    
    init(data: String) {
        self.qrCodeData = data
    }
    
    func toDictionary() -> [String: String] {
        do {
            let data: Data = try JSONEncoder().encode(self)
            if let dict: [String: String] = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String] {
                return dict
            }
            return [:]
        } catch {
            print(error.localizedDescription)
            return [:]
        }
    }
    
}
